name: xeno
version: 1.0.0.0
synopsis: A fast event-based XML parser in pure Haskell
description: A fast, low-memory use, event-based XML parser in pure Haskell.  
build-type: Simple
category: XML, Parser
cabal-version: >=1.10
homepage: https://github.com/ocramz/xeno
license: BSD3
license-file: LICENSE
author: Christopher Done
maintainer: Marco Zocca (ocramz fripost org)
tested-with:         GHC == 8.0.1, GHC == 8.2.2, GHC == 8.4.2, GHC == 8.4.4
extra-source-files:  README.md
                     CHANGELOG.markdown
                     CONTRIBUTORS.md                             

source-repository head
  type:     git
  location: https://github.com/ocramz/xeno
                     
flag libxml2
  description:   Include libxml2 in the benchmarks
  default:       False

library
  hs-source-dirs: src
  ghc-options: -Wall -O2
  exposed-modules: Xeno.SAX, Xeno.DOM, Xeno.Types, Xeno.Errors, Xeno.DOM.Robust
  other-modules: Control.Spork, Xeno.DOM.Internal
  build-depends: base >= 4.7 && < 5
               , bytestring
               , vector
               , deepseq
               , mutable-containers
               , mtl
               -- , exceptions
               -- | DEBUG 
               -- , hspec
  default-language: Haskell2010

test-suite xeno-test
           
  type: exitcode-stdio-1.0
  hs-source-dirs: test
  main-is: Main.hs
  build-depends: base, xeno, hexml, hspec, bytestring
  ghc-options: -Wall -threaded -rtsopts -with-rtsopts=-N
  default-language: Haskell2010

benchmark xeno-speed-bench
  type: exitcode-stdio-1.0
  hs-source-dirs: bench
  main-is: Speed.hs
  build-depends: base, xeno, hexml, criterion, bytestring, deepseq, ghc-prim, xml, hexpat
  if flag(libxml2)
    build-depends: libxml
  -- ghc-options: -Wall -threaded -rtsopts -O2 -with-rtsopts=-N
  ghc-options: -Wall -O2 -rtsopts
  if flag(libxml2)
     cpp-options: -DLIBXML2   
     -- ghc-options: -DLIBXML2  -- Hackage started complaining about this
  default-language: Haskell2010

benchmark xeno-memory-bench
  type: exitcode-stdio-1.0
  hs-source-dirs: bench
  main-is: Memory.hs
  build-depends: base, xeno, weigh, bytestring, deepseq, hexml
  -- ghc-options: -Wall -threaded -O2 -rtsopts -with-rtsopts=-N
  ghc-options: -Wall -O2 -rtsopts
  default-language: Haskell2010

benchmark xeno-speed-big-files-bench
  type: exitcode-stdio-1.0
  hs-source-dirs: bench
  main-is: SpeedBigFiles.hs
  build-depends: base, xeno, hexml, criterion, bytestring, deepseq, ghc-prim, xml, hexpat, bzlib, filepath
  if flag(libxml2)
    build-depends: libxml
  ghc-options: -Wall -O2 -rtsopts "-with-rtsopts=-H10G -AL1G -A256m -M25G"
  -- ghc-options: -Wall -O2 -threaded -rtsopts "-with-rtsopts=-N -H10G -AL1G -A256m -M25G"
  if flag(libxml2)
     cpp-options: -DLIBXML2
  default-language: Haskell2010

executable xeno-bench
  main-is: Bench.hs
  hs-source-dirs: app
  build-depends: base, xeno, weigh, bytestring, deepseq, hexml, bytestring-mmap, time, interpolatedstring-perl6
  ghc-options: -O2 -threaded -rtsopts "-with-rtsopts=-N"
  default-language: Haskell2010
